// Теоретичне питання:
// Рекурсія це коли функція визиває саму себе углиб допоки не виконає умови функції.
// Вона інколи може використовуватися в деревовидних структурах замість ітерації як простіше та швидше рішення.
// Завдання:

let naturalNumber;
while (
  !naturalNumber ||
  Math.floor(naturalNumber) !== naturalNumber ||
  Math.sign(naturalNumber) === -1) {
  naturalNumber = +prompt("Enter your natural number");
}
function factorial(n) {
  if (n === 1) {
    return 1;
  } else {
    return n * factorial(n - 1);
  }
}
console.log(factorial(naturalNumber));
